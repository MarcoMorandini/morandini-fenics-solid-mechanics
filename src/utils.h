// Copyright (C) 2009-2012 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2009-10-02
// Last changed: 2012-07-17

#ifndef __FENICS_SOLID_UTILS_H
#define __FENICS_SOLID_UTILS_H

#include <string>
#include <vector>
#include <Eigen/Dense>
#include <boost/multi_array.hpp>

namespace dolfin
{
  class FiniteElement;
}

namespace fenicssolid
{

  /// Return git commit hash for library
  std::string git_commit_hash();

  /// Compute strain a point in real space
  void compute_strain(Eigen::Matrix<double, 6, 1>& strain,
                      const dolfin::FiniteElement& element,
                      const double* vertex_coordinates,
                      const double* coords,
                      const std::vector<double>& expansion_coeffs);

  /// Compute strain a point in reference element
  void
    compute_strain_reference(Eigen::Matrix<double, 6, 1>& strain,
                             const boost::multi_array<double, 2>& derivatives,
                             const double* vertex_coordinates,
                             const std::vector<double>& expansion_coeffs);

  /// Compute position of a point in reference coordinates
  void compute_reference_x_3(std::vector<double>& X,
                             const double* vertex_coordinates,
                             const double* x);

  /// Compute shape function derivatives on reference element
  // The function signature is clumsy because UFC does not yet provide
  // a function for evaluating basis functions directly on the
  // reference element. This functions maps UFC output back to the
  // reference.
  //boost::multi_array<double, 3>
  //  compute_basis_derivatives1(const dolfin::FiniteElement& element,
  //                             const boost::multi_array<double, 2>& coordinates,
  //                              const dolfin::UFCCell& cell);
  void
    compute_basis_derivatives1(const dolfin::FiniteElement& element,
                               const boost::multi_array<double, 2>& coordinates,
                               const double* vertex_coordinates,
                               boost::multi_array<double, 3>& derivs);

};

#endif
